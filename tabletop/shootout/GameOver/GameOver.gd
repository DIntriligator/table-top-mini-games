extends CanvasLayer

signal new_game
onready var current_game_state = ShootoutGameState.current_game_state

func _ready(): 
	set_winner_text()

func _enter_tree():
	current_game_state = ShootoutGameState.current_game_state
	set_winner_text()

func _on_Modal_confirm():
	emit_signal("new_game")

func set_winner_text():
	$Modal.is_open = true
	$Modal.hide_close = true
	$Modal.set_color_from_hex(current_game_state.winner.color)
	$Modal.set_font_color_from_hex('#ffffff')
	$Modal/Confirm.set_color_from_hex(Color(current_game_state.winner.color).darkened(0.3))
	$Modal.content = "Player 1 Wins!" if current_game_state.winner.id == 0 else "Player 2 Wins!"
	$Modal.confirm_label = "New Game"



