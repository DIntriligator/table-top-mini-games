extends CanvasLayer

onready var current_game_state = ShootoutGameState.current_game_state

signal complete

func _ready():
	set_panel_color()

func _enter_tree():
	current_game_state = ShootoutGameState.current_game_state
	set_label()
	set_panel_color()
	
func _on_Timer_timeout():
	emit_complete()
	
func _on_Modal_confirm():
	emit_complete()
	
func set_label():
	var player_text = "Player 1" if ShootoutGameState.get_attacking_player_id() == 0 else "Player 2"
	var scored_text = "Scored!" if current_game_state.current_turn.scored else "Missed!"
	$Modal.content = player_text + "\n" + scored_text
	
func set_panel_color():
	$Modal.is_open = true
	$Modal.hide_close = true
	$Modal.set_color_from_hex(current_game_state.current_turn.attack_player.color)
	$Modal/Confirm.set_color_from_hex(Color(current_game_state.current_turn.attack_player.color).darkened(0.3))
	$Modal.set_font_color_from_hex('#ffffff')

func emit_complete():
	$Timer.stop()
	emit_signal("complete")
