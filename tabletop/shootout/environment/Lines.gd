extends Control

onready var current_game_state = ShootoutGameState.current_game_state

func _ready():
	rect_pivot_offset = Globals.half_screen_size
	connect_to_state_update()

func connect_to_state_update():
	var _connection = ShootoutGameState.connect("state_updated", self, "_on_state_update")
	
func _on_state_update():
	current_game_state = ShootoutGameState.current_game_state
	var is_player_1_attacking = current_game_state.current_turn.attack_player and current_game_state.current_turn.attack_player.id == 0
	rect_rotation = 0 if is_player_1_attacking else 180
