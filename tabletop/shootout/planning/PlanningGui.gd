extends CanvasLayer

signal confirmed
signal toggle_confirm_modal
onready var modal = $ConfirmModal
onready var current_game_state = ShootoutGameState.current_game_state

func _ready():
	set_botton_color()
	set_modal_color()

func _enter_tree():
	current_game_state = ShootoutGameState.current_game_state
	set_player_rotation()
	set_button_position()
	set_botton_color()
	set_modal_color()

func set_player_rotation():
	rotation_degrees = 0 if ShootoutGameState.is_player_1_active_planning() else 180
	offset = Vector2() if ShootoutGameState.is_player_1_active_planning() else get_viewport().get_visible_rect().size

func set_button_position():
	$OkButton.rect_position.x = -240
	if get_viewport().get_visible_rect().size.y > 900:
		$OkButton.rect_size.y = 150
	$OkButton.rect_position.y = Globals.half_screen_size.y + Globals.screen_origin.y - $OkButton.rect_size.y

func _on_OkButton_pressed():
	modal.content = "Are you done setting \nyour turn?"
	modal.is_open = true
	modal.confirm_label = "Yes"
	emit_signal("toggle_confirm_modal", true)


func _on_ConfirmModal_cancel():
	modal.is_open = false
	emit_signal("toggle_confirm_modal", false)
	
func set_botton_color():
	$OkButton.set_color_from_hex(Color(current_game_state.planning_active_player.color).darkened(0.2))
	
func set_modal_color():
	$ConfirmModal.set_font_color_from_hex("#ffffff")
	$ConfirmModal.set_color_from_hex(current_game_state.planning_active_player.color)
	$ConfirmModal/Confirm.set_color_from_hex(Color(current_game_state.planning_active_player.color).darkened(0.3))

func _on_ConfirmModal_confirm():
	modal.is_open = false
	emit_signal("confirmed")
	emit_signal("toggle_confirm_modal", false)
