extends Control

signal done_planning
onready var current_game_state = ShootoutGameState.current_game_state

func _ready():
	set_player()
	rect_pivot_offset = Globals.half_screen_size

func _enter_tree():
	current_game_state = ShootoutGameState.current_game_state
	set_player()
	
func _on_PlanningGui_confirmed():
	var pos = get_final_position()
	var rot = get_final_rotation()
	set_actor_attributes(pos, rot)
	emit_done()

func set_player():
	rect_rotation = 0 if ShootoutGameState.is_player_1_active_planning() else 180

func get_final_position():
	var actor_pos = $Positioning.actor_position
	if(ShootoutGameState.is_player_1_active_planning()): 
		return actor_pos + Globals.half_screen_size
	else:
		return Globals.screen_size - (actor_pos + Globals.half_screen_size)

func get_final_rotation():
	if ShootoutGameState.is_player_1_active_planning():
		return $Positioning.actor_rotation
	else:
		return fmod($Positioning.actor_rotation - 180, 360)
	
func set_actor_attributes(actor_position, actor_rotation):
	if ShootoutGameState.is_attack_planning_turn():
		ShootoutGameState.update_turn("attack_pos", actor_position)
		ShootoutGameState.update_turn("attack_rot", actor_rotation)
	else:
		ShootoutGameState.update_turn("defense_pos", actor_position)
		ShootoutGameState.update_turn("defense_rot", actor_rotation)

func emit_done():
	emit_signal("done_planning")
