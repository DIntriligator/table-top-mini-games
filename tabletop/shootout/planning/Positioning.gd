extends Control

onready var current_game_state = ShootoutGameState.current_game_state

var is_rotate_mode = false
var actor
var initial_position = Vector2(0, 150)
var actor_position = initial_position
var actor_rotation = 0
var is_player_1 = true
var disabled = false

var ball_scene = preload("res://tabletop/shootout/actors/Ball.tscn").instance()
var goalie_scene = preload("res://tabletop/shootout/actors/Goalie.tscn").instance()

func _ready():
	set_player()
	
func _enter_tree():
	set_player()
	clear_actor()
	set_actor()
	show_directional_arrow()
	reset_actor_pos()
	reset_actor_rot()

func _input(event): 
	check_rotating_input(event)
	
func _on_PlanningGui_toggle_confirm_modal(is_open):
	set_disabled(is_open)

func set_player():
	is_player_1 = ShootoutGameState.is_player_1_active_planning()

func clear_actor():
	if actor:
		remove_child(actor)

func set_actor():
	if ShootoutGameState.is_attack_planning_turn():
		actor = ball_scene
		add_child(ball_scene)
	else: 
		actor = goalie_scene
		add_child(goalie_scene)

func check_rotating_input(event):
	if event is InputEventScreenDrag and not disabled:
		var is_outside_of_button_area = get_is_outside_of_button_area(event)
		if is_outside_of_button_area:
			check_rotate_or_drag(event)
			
func check_rotate_or_drag(event):
	var is_rotating = get_rotation_is_far_enough(event)
	if is_rotating:
		set_actor_rot_on_drag(event)
	else:
		set_actor_position_on_drag(event)
		

func reset_actor_pos():
	actor.position =  initial_position
	actor_position = initial_position
		
func set_actor_position_on_drag(event): 
	actor.position = check_get_actor_position(event)
	actor_position = actor.position

func check_get_actor_position(event):
	var new_postion = Vector2(0,0)
	var event_position = get_centered_event_position(event) if is_player_1 else get_centered_flipped_event_position(event)
	new_postion.x = get_clamped_position_x(event_position)
	new_postion.y = get_clamped_position_y(event_position)
	return new_postion
	
func get_clamped_position_x(event_position):
	return clamp(event_position.x, actor.get_half_width() - Globals.half_screen_size.x, (Globals.screen_size.x - actor.get_half_width() - Globals.half_screen_size.x))

func get_clamped_position_y(event_position):
	return clamp(event_position.y, 100, (Globals.screen_size.y - 150 - Globals.half_screen_size.y))

func get_centered_event_position(event):
	return event.position - Globals.half_screen_size - Globals.screen_origin

func get_centered_flipped_event_position(event):
	return Globals.screen_size - event.position - Globals.half_screen_size + Globals.screen_origin
		
func reset_actor_rot(): 
	if actor and actor.sprite:
		actor.sprite.rotation_degrees = 0
		if actor.directional_arrow:
			actor.directional_arrow.rotation_degrees = 0
	actor_rotation = 0
	
func set_actor_rot_on_drag(event):
	if event is InputEventScreenDrag and not disabled:
		var is_outside_of_button_area = get_is_outside_of_button_area(event)
		var is_far_enough = get_rotation_is_far_enough(event)
		if is_outside_of_button_area and is_far_enough:
			rotate_actor(event)
			
func rotate_actor(event):
	var new_rotation = fmod(floor(rad2deg(actor.get_angle_to(event.position))) + 90, 360)
	actor.sprite.rotation_degrees = new_rotation
	if actor.directional_arrow:
			actor.directional_arrow.rotation_degrees = new_rotation
	actor_rotation = new_rotation
		
func get_is_outside_of_button_area(event):
	if is_player_1:
		return event.position.y < Globals.screen_origin.y + Globals.screen_size.y - 100
	else:
		return event.position.y >  Globals.screen_origin.y + 100

func get_rotation_is_far_enough(event):
	return event.position.distance_to(actor.get_global_position()) > actor.get_half_width() * 1.5

func set_disabled(is_disabled):
	disabled = is_disabled;
	
func show_directional_arrow():
	if actor:
		actor.show_arrow = true
