extends CanvasLayer

signal on_ready
onready var current_game_state = ShootoutGameState.current_game_state

func _ready():
	set_label()
	set_panel_color()

func _enter_tree():
	current_game_state = ShootoutGameState.current_game_state
	set_label()
	set_scene_rotation()
	set_panel_color()
	
func _on_Modal_confirm():
	emit_signal("on_ready")
	
func set_label():
	var player_text = "Player 1's Turn" if ShootoutGameState.is_player_1_active_planning() else "Player 2's Turn"
	var round_text = "Round " + str(current_game_state.turn_index + 1)
	$Modal.hide_close = true
	$Modal.content = player_text + "\n" + round_text
	$Modal.is_open = true
	$Modal.confirm_label = "Ready!"
	$Modal.set_font_color_from_hex('#ffffff')

func set_scene_rotation():
	if ShootoutGameState.is_player_1_active_planning():
		$Modal.rect_rotation = 0
	else: 
		$Modal.rect_rotation = 180

func set_panel_color():
	$Modal.set_color_from_hex(current_game_state.planning_active_player.color)
	$Modal/Confirm.set_color_from_hex(Color(current_game_state.planning_active_player.color).darkened(0.3))
	$Modal.set_font_color_from_hex('#ffffff')
