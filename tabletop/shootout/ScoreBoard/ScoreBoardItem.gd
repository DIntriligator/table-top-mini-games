extends Control

var circle_color = Color('#ff0000') setget set_circle_color
var danger_color = Color('#b1242c')
var success_color = Color('#036e07')
var state = 0 setget set_state
var size = 37


func _ready():
	$Goal.modulate = success_color
	$Miss.modulate = danger_color
	$Circle.modulate = circle_color


func set_circle_color(color):
	circle_color = color
	$Circle.modulate = Color(color).darkened(0.2)
	
func set_state(state):
	match(state):
		1: 
			$Goal.hide()
			$Miss.show()
			$Circle.show()
			$Bg.modulate = Color('#ffffff')
		2: 
			$Goal.show()
			$Miss.hide()
			$Circle.show()
			$Bg.modulate = Color('#ffffff')
		_:
			$Goal.hide()
			$Miss.hide()
			$Circle.show()
			$Bg.modulate = Color(circle_color)
