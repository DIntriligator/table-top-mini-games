extends Control

export (PackedScene) var ScoreBoardItem
var items = []
var player = null setget set_scores
var team_color setget set_team_color
onready var current_game_state = ShootoutGameState.current_game_state

func _ready():
	connect_to_state_update()
	set_scores(player)	

func connect_to_state_update():
	var _connection = ShootoutGameState.connect("state_updated", self, "_on_state_update")
	
func _on_state_update():
	current_game_state = ShootoutGameState.current_game_state
	set_team_scores(current_game_state.players[player].scores)
	
func set_scores(player_id): 
	if player_id != null:
		player = player_id
		set_team_scores(current_game_state.players[player_id].scores)
		set_team_color(current_game_state.players[player_id].color)

func set_team_scores(scores):
	if player != null:
		var i = 0
		check_set_new_items(scores)
		for score in scores: 
			if items[i]:
				items[i].state = score
			i += 1

func set_team_color(color):
	for item in items:
		if item:
			item.circle_color = color
		
func check_set_new_items(scores):
	if(scores.size() != items.size()):
		clear_items()
		var i = 0
		for score in scores:
			var new_item = ScoreBoardItem.instance()
			items.append(new_item)
			add_child(new_item)
			get_new_position(new_item, i)
			i += 1
	set_team_color(current_game_state.players[player].color)

func get_new_position(item, i):
	var x_mod = ceil((i) / 5)
	item.rect_position.y += item.size * (i % 5)
	if player == 0:
		item.rect_position.x -= item.size * x_mod
	else:
		item.rect_position.x += item.size * x_mod
	
func clear_items():
	for item in items:
		if item: 
			item.queue_free()
	items = []
