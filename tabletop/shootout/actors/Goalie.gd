extends StaticBody2D

onready var sprite = $Sprite
var directional_arrow = null
var show_arrow = false 
export var soft = false

onready var current_game_state = ShootoutGameState.current_game_state

func _ready():
	set_tint()

func _enter_tree():
	current_game_state = ShootoutGameState.current_game_state
	set_tint()

func get_half_width():
	return ($Sprite.texture.get_size().x / 2)

func set_tint():
	var tint = Color(current_game_state.current_turn.defense_player.color)
	$Sprite.modulate = tint
