extends KinematicBody2D

onready var sprite = $AnimatedSprite
onready var directional_arrow = $DirectionalArrow
signal ball_stopped

var velocity = Vector2(0,-1)
var initial_speed = 800
var speed = 0
var bounce_speed_decrease_amount = 300
var speed_dampen_factor = 0.04
var should_emit_stopped = false
var show_arrow = false
var arrow_position = 0
onready var current_game_state = ShootoutGameState.current_game_state

func _ready():
	set_arrow_visibility()
	set_initial_rotation()

func _enter_tree():
	current_game_state = ShootoutGameState.current_game_state
	set_tint()

func _physics_process(delta):
	if speed > 0: 
		$AnimatedSprite.play()
		$AnimatedSprite.speed_scale = speed / 100
		move_ball(delta)
		dampen_speed()
	else:
		$AnimatedSprite.speed_scale = 0
		emit_stopped()
	
func _process(delta):
	if show_arrow:
		directional_arrow.position = Vector2(0, cos(int(arrow_position)) * 6 - 10).rotated($AnimatedSprite.rotation)
		arrow_position += 0.12

func _on_VisibilityNotifier2D_viewport_exited(_viewport):
	stop()
	emit_stopped()

func move_ball(delta):
	velocity = get_velocity()
	var collision_info = move_and_collide(velocity * delta, false)
	check_bounce_on_collision(collision_info)

func get_velocity():
	return velocity.normalized() * speed

func check_bounce_on_collision(collision_info):
	if collision_info:
		bounce_on_collision(collision_info)
		check_slow_down_on_soft(collision_info)
		

func bounce_on_collision(collision_info):
	velocity = velocity.bounce(collision_info.normal)
	turn_to_face_velocity(velocity)
	decrease_speed_on_bounce()
	
func turn_to_face_velocity(new_velocity):
	$AnimatedSprite.rotation_degrees = rad2deg((Vector2(0,0).angle_to_point(new_velocity)))
	
func decrease_speed_on_bounce():
	speed -= bounce_speed_decrease_amount
	
func check_slow_down_on_soft(collision_info):
	if collision_info.get_collider().soft:
		speed = 150 if speed > 150 else speed

func dampen_speed(): 
	speed -= (initial_speed * 1.01 - speed) * speed_dampen_factor

func start():
	velocity = Vector2(0,-1).rotated(rotation)
	speed = initial_speed
	should_emit_stopped = true
	
func stop():
	speed = 0

func emit_stopped():
	if should_emit_stopped:
		emit_signal("ball_stopped")
	should_emit_stopped = false
	
func get_half_width():
	return 25

func set_tint():
	var tint = Color(current_game_state.current_turn.attack_player.color).darkened(0.3)
	$AnimatedSprite.modulate = tint
	$DirectionalArrow.modulate = tint

func set_arrow_visibility():
	if show_arrow:
		$DirectionalArrow.show()
	else:
		$DirectionalArrow.hide()
		
func set_initial_rotation():
	$AnimatedSprite.rotation = rotation
