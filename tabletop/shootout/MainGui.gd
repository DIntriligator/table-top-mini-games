extends CanvasLayer

onready var current_game_state = ShootoutGameState.current_game_state

func _ready():
	set_score_board()

func set_score_board():
	$Player1Scores.team_color = Color(current_game_state.players[0].color)
	$Player1Scores.player = 0
	$Player2Scores.team_color = Color(current_game_state.players[1].color)
	$Player2Scores.player = 1
	
	
