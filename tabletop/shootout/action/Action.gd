extends Control

onready var current_game_state = ShootoutGameState.current_game_state

signal complete

func _enter_tree():
	current_game_state = ShootoutGameState.current_game_state
	set_actors()
	
func _on_Timer_timeout():
	start_action()

func _on_Ball_ball_stopped():
	check_scored()
	
func _on_EndDelay_timeout():
	end_action_phase()

func start():
	$Timer.start()

func set_actors():
	set_ball_pos()
	set_ball_rot()
	set_goalie_pos()
	set_goalie_rot()

func end_action_phase():
	emit_signal("complete")
	$Timer.stop()
	$EndDelay.stop()

func set_ball_pos():
	$Ball.position = current_game_state.current_turn.attack_pos

func set_ball_rot():
	$Ball.rotation_degrees = current_game_state.current_turn.attack_rot
	
func set_goalie_pos():
	$Goalie.position = current_game_state.current_turn.defense_pos
	
func set_goalie_rot():
	$Goalie.rotation_degrees = current_game_state.current_turn.defense_rot
	
func start_action(): 
	$Ball.start()

func check_scored():
	var player_1_attacking = ShootoutGameState.get_attacking_player_id() == 0
	var ball_global_pos = $Ball.get_global_position()
	var ball_is_touching_top = ball_global_pos.y < (Globals.screen_origin.y + $Ball.get_half_width()) + 110
	var ball_is_touching_bottom = ball_global_pos.y  < (Globals.screen_origin.y + Globals.screen_size.y - $Ball.get_half_width()) - 110
	var player_1_attack_win = player_1_attacking and ball_is_touching_top
	var player_1_defense_win = not player_1_attacking and ball_is_touching_bottom
	var player_1_win = player_1_attack_win or player_1_defense_win
	update_score(player_1_win, player_1_attacking)
	$EndDelay.start()

func update_score(player_1_win, player_1_attacking):
	if player_1_win and player_1_attacking:
		player_add_scored(0)
	if not player_1_win and player_1_attacking:
		player_add_missed(0)
	if not player_1_win and not player_1_attacking: 
		player_add_scored(1)
	if player_1_win and not player_1_attacking: 
		player_add_missed(1)

func player_add_scored(player_index):
	ShootoutGameState.add_player_scored(player_index)

func player_add_missed(player_index):
	ShootoutGameState.add_player_missed(player_index)
