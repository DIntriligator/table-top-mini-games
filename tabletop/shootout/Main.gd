extends Control

onready var current_game_state = ShootoutGameState.current_game_state
var should_increase_turn = false;

var planning = preload("res://tabletop/shootout/planning/Planning.tscn").instance()
var ready_message = preload("res://tabletop/shootout/GetReadyMessage/GetReadyMessage.tscn").instance()
var action = preload("res://tabletop/shootout/action/Action.tscn").instance()
var game_over = preload("res://tabletop/shootout/GameOver/GameOver.tscn").instance()
var turn_end = preload("res://tabletop/shootout/TurnEnd/TurnEnd.tscn").instance()


func _ready():
	show_ready_message()
	connect_to_state_update()
	connect_to_planning_done()
	connect_to_ready_message_done()
	connect_to_action_complete()
	connect_to_new_game()
	connect_to_turn_end()

func connect_to_state_update():
	var _connection = ShootoutGameState.connect("state_updated", self, "_on_state_update")
	
func connect_to_planning_done():
	var _connection = planning.connect("done_planning", self, "_on_Planning_done_planning")
	
func connect_to_ready_message_done():
	var _connection = ready_message.connect("on_ready", self, "_on_GetReadyMessage_on_ready")
	
func connect_to_action_complete():
	var _connection = action.connect("complete", self, "_on_Action_complete")
	
func connect_to_new_game():
	var _connection = game_over.connect("new_game", self, "_on_GameOver_new_game")
	
func connect_to_turn_end():
	var _connection = turn_end.connect("complete", self, "_on_TurnEnd_complete")

func _on_state_update():
	current_game_state = ShootoutGameState.current_game_state

func _on_GetReadyMessage_on_ready():
	hide_ready_message()
	show_planning()

func _on_Planning_done_planning():
	hide_planning()
	check_show_action()

func _on_Action_complete():
	hide_action()	
	check_win()
	

func _on_TurnEnd_complete():
	reset_turn_state()
	hide_turn_end()
	check_add_sudden_death_turn()
	toggle_player()
	check_increase_turn_index()
	set_attack_planning()
	show_ready_message()
	
func _on_GameOver_new_game():
	ShootoutGameState.reset_state()
	hide_game_over()
	show_planning()

func check_show_action():
	if is_planning_over():
		show_action()
	else:
		set_defense_planning()
		show_ready_message()

func is_planning_over():
	return !ShootoutGameState.is_attack_planning_turn()

func toggle_player():
	if not current_game_state.is_end_of_turn:
		ShootoutGameState.switch_attacking_defending_players()

func hide_planning():
	remove_child(planning)

func show_planning():
	add_child(planning)
	
func set_attack_planning():
	ShootoutGameState.set_planning_active_player_to_attack_player()

func set_defense_planning():
	ShootoutGameState.set_planning_active_player_to_defense_player()

func hide_ready_message():
	remove_child(ready_message)

func show_ready_message():
	add_child(ready_message)

func hide_turn_end():
	remove_child(turn_end)

func show_turn_end():
	add_child(turn_end)

func hide_action():
	remove_child(action)
	
func show_action():
	add_child(action)
	action.start()
	
func hide_game_over():
	remove_child(game_over)

func show_game_over():
	add_child(game_over)

func check_increase_turn_index():
	if current_game_state.is_end_of_turn:
		ShootoutGameState.increase_turn_index()
	ShootoutGameState.toggle_is_end_of_turn()

func check_win():
	if current_game_state.winner:
		show_game_over()
	else:
		show_turn_end()
	
func check_add_sudden_death_turn():
	if current_game_state.turn_index > 3 and current_game_state.is_end_of_turn:
		ShootoutGameState.add_new_score_box()
		
func reset_turn_state():
	ShootoutGameState.update_turn("scored", false)
