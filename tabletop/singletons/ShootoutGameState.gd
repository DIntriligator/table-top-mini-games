extends Node


signal state_updated

var player_1 = {
		"id": 0,
		"scores": [ 0, 0, 0, 0, 0],
		"total_scores": 0,
		"attempts_left": 5,
		"color": "#0027FC"
	}
var player_2 = {
		"id": 1,
		"scores": [ 0, 0, 0, 0, 0],
		"total_scores": 0,
		"attempts_left": 5,
		"color": "#FE9000"
	}
var initial_game_state = {
	"players": [
		player_1,
		player_2
	],
	"turn_index": 0,
	"is_end_of_turn": false,
	"planning_active_player": player_1,
	"planning_inactive_player": player_2,
	"winner": null,
	"current_turn": {
		"scored": false,
		"attack_player": player_1,
		"defense_player": player_2,
		"attack_pos": Vector2(0,0),
		"attack_rot": 90,
		"defense_pos": Vector2(0,0),
		"defense_rot": 90
	}
}
var current_game_state = initial_game_state.duplicate(true)

func _ready():
	reset_state()

func reset_state():
	current_game_state = initial_game_state.duplicate(true)
	set_attacking_player(0)
	set_defending_player(1)
	set_planning_active_player_to_attack_player()
	emit_updated()

func set_attacking_player(player_index):
	current_game_state.current_turn.attack_player = current_game_state.players[player_index]
	emit_updated()

func set_defending_player(player_index):
	current_game_state.current_turn.defense_player = current_game_state.players[player_index]
	emit_updated()
	
func get_attacking_player_id():
	if current_game_state.current_turn.attack_player:
		return current_game_state.current_turn.attack_player.id
	else:
		return null

func get_defending_player_id():
	if current_game_state.current_turn.defense_player:
		return current_game_state.current_turn.defense_player.id
	else:
		return null

func get_winner():
	return current_game_state.winner

func update_player(index, key, value):
	current_game_state.players[index][key] = value
	emit_updated()
	
func update_turn(key, value):
	current_game_state.current_turn[key] = value
	emit_updated()

func set_planning_active_player_to_attack_player():
	current_game_state.planning_active_player = current_game_state.current_turn.attack_player
	current_game_state.planning_inactive_player = current_game_state.current_turn.defense_player
	emit_updated()

func set_planning_active_player_to_defense_player():
	current_game_state.planning_active_player = current_game_state.current_turn.defense_player
	current_game_state.planning_inactive_player = current_game_state.current_turn.attack_player
	emit_updated()

func switch_attacking_defending_players():
	if current_game_state.current_turn.attack_player.id == 1:
		current_game_state.current_turn.attack_player = current_game_state.players[0]
		current_game_state.current_turn.defense_player = current_game_state.players[1]
	else:
		current_game_state.current_turn.attack_player = current_game_state.players[1]
		current_game_state.current_turn.defense_player = current_game_state.players[0]
	emit_updated()

func increase_turn_index():
	current_game_state.turn_index = current_game_state.turn_index + 1
	emit_updated()
	
func is_attack_planning_turn():
	return current_game_state.current_turn.attack_player == current_game_state.planning_active_player
	
func is_player_1_active_planning():
	return current_game_state.planning_active_player and current_game_state.planning_active_player.id == 0
	
func add_new_score_box():
	current_game_state.players[0].scores.append(0)
	current_game_state.players[1].scores.append(0)
	emit_updated()

func toggle_is_end_of_turn():
	current_game_state.is_end_of_turn = !current_game_state.is_end_of_turn
	emit_updated()
	
func add_player_scored(player_index):
	var player = current_game_state.players[player_index] 
	player.scores[current_game_state.turn_index] = 2
	player.total_scores += 1
	player.attempts_left -= 1
	ShootoutGameState.update_turn("scored", true)
	check_winner()
	emit_updated()
	
func add_player_missed(player_index):
	var player = current_game_state.players[player_index] 
	player.scores[current_game_state.turn_index] = 1
	player.attempts_left -= 1
	check_winner()
	emit_updated()

func check_winner(): 
	var player_1_lost = check_player_lost(0, 1)
	var player_2_lost = check_player_lost(1, 0)
	if player_1_lost:
		current_game_state.winner = current_game_state.players[1]
	if player_2_lost:
		current_game_state.winner = current_game_state.players[0]

func check_player_lost(player_to_check_index, other_player_index):
	var player_to_check = current_game_state.players[player_to_check_index]
	var player_to_check_score = player_to_check.total_scores
	var player_other_player_score = current_game_state.players[other_player_index].total_scores
	var turns_left = player_to_check.attempts_left
	var score_diff =  player_other_player_score - player_to_check_score 
	var normal_loss = turns_left > 0 and score_diff > turns_left
	var sudden_death_loss = current_game_state.is_end_of_turn and turns_left <= 0 and player_to_check_score < player_other_player_score
	return normal_loss || sudden_death_loss

func emit_updated():
	emit_signal("state_updated")

