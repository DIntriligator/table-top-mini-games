extends Node

var half_screen_size
var screen_origin = Vector2()
onready var viewport = get_viewport()
onready var screen_size = Vector2(
	ProjectSettings.get_setting("display/window/size/width"),
	ProjectSettings.get_setting("display/window/size/height"))

func _ready():
	screen_size =Vector2(480, 720)
	half_screen_size = Vector2(screen_size.x / 2, screen_size.y / 2)
	viewport.connect("size_changed", self, "resize_viewport")
	resize_viewport()

func resize_viewport():
	var new_size = OS.get_window_size()
	var scale_factor
	scale_factor = screen_size.x/new_size.x
	new_size = Vector2(new_size.x*scale_factor, new_size.y*scale_factor)
	screen_origin = ( new_size - screen_size) /2
	viewport.set_size_override(true, new_size)
