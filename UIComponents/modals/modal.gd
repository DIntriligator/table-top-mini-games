extends Panel

signal confirm
signal cancel
export var content = '' setget set_content
export var is_open = false setget set_open
export var confirm_label = 'Confirm' setget set_confirm_label
export var hide_close = false setget hide_close_button
var panel_style = StyleBoxFlat.new()
var default_color = "#ffffff"

func _ready():
	set_open(is_open)
	set_default_style()

func _on_DangerButton_pressed():
	emit_signal("cancel")

func _on_Close_pressed():
	emit_signal("cancel")

func _on_Confirm_pressed():
	emit_signal("confirm")
	
func _process(_delta):
	rect_size.y = $Label.rect_size.y + 150
	
func set_default_style():
	panel_style.corner_radius_bottom_left = 5
	panel_style.corner_radius_bottom_right = 5
	panel_style.corner_radius_top_left = 5
	panel_style.corner_radius_top_right = 5
	panel_style.border_width_bottom = 2
	panel_style.border_width_top = 2
	panel_style.border_width_right = 2
	panel_style.border_width_left = 2
	set_color_from_hex(default_color);
	set('custom_styles/panel', panel_style)

func set_content(new_content):
	$Label.text = new_content;

func set_open(open):
	if open: 
		show() 
	else: 
		hide()

func hide_close_button(should_hide):
	if should_hide:
		$Close.hide()
	else:
		$Close.show()

func set_confirm_label(new_text):
	$Confirm.text = new_text

func set_color_from_hex(new_color):
	panel_style.bg_color = Color(new_color)
	panel_style.border_color = Color(new_color).darkened(0.2)
	
func set_font_color_from_hex(new_color):
	$Label.add_color_override("font_color", Color(new_color))

